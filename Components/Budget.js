import React from 'react'
import { StyleSheet, View, TextInput, Button, FlatList, Text, Image} from 'react-native'
import depense from '../Helpers/depenseData'
import Depense from './Depense'

class Budget extends React.Component {

  render(){

    return(

      <View style={styles.main_container}>

        <View style={styles.header_container}>

          <View style={styles.account_container}>

            {/*<Image 

              style={styles.image}
              source={require('../assets/test.jpg')}

            />*/}

            <Text style={styles.accountName}>

            Aimé
            
            </Text>
            
          </View>

          <View style={styles.totalBudget_container}>

            <Text style={styles.totalText}>TOTAL Sur compte :</Text>

            <Text style={styles.budgetText}>100$</Text>
            
          </View>

        </View>

        <View style={styles.list_container}>

          <FlatList

            data={depense}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => <Depense values={item}/>}

          />

        </View>
      </View>

    )

  }

}

const styles = StyleSheet.create({

  main_container: {

    flex: 1,
    padding: 5,
    backgroundColor: '#F4F4F4'
   
  },

  header_container: {

    
    backgroundColor: '#FFFFFF',
    height: 100,
    margin: 10,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
    
  },

  account_container: {

    flex: 1,
    flexDirection: 'row-reverse',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',

  },

  totalBudget_container: {

    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'

  },

  list_container: {

    flex: 1,
    margin: 5,
    borderRadius: 20,
    padding: 10,
    backgroundColor: '#FFFFFF',
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6, 

  },


  image: {

    flex: 3,
    minHeight: 80,
    minWidth : 80,
    maxHeight: 80,
    maxWidth: 80,
    backgroundColor: 'grey',
    borderRadius: 500,
    margin: 5

  },

  accountName: {

    flex: 6,
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black'
    
  },

  totalText: {
    
    flex: 1,
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: "center",
    color: 'black'

  },

  budgetText:{

    flex: 1,
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: "center",
    color: 'green'

  }
})

export default Budget