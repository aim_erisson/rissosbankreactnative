import React from 'react'
import { StyleSheet, View, TextInput, Button, FlatList, Text, Image} from 'react-native'

class Depense extends React.Component {

  render(){

    const value = this.props.values

    return(

      <View style={styles.main_container}>

        <Image 

          style={styles.image}
          source={{uri: 'image'}}
      
        />

        <View style={styles.infos_container}>

          <Text style={styles.comment}>{value.comment}</Text>
          <Text style={styles.date}>{value.creation_date}</Text>

        </View>

        <Text>{value.value}</Text>

      </View>

    )

  }

}

const styles = StyleSheet.create({

  main_container: {

    height: 50,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#97abb5',
    backgroundColor: '#C7F4C3',
    borderRadius: 5,
    alignItems: 'center'
    

  },

  infos_container: {

    flex: 1

  },

  image: {

    flex: 1,
    maxWidth: 20,
    maxHeight: 20,
    borderRadius: 50,
    backgroundColor: 'red',
    alignItems: 'center',
    textAlign: "center"
  },

  comment: {

    flex: 1

  },

  date: {

    flex: 1

  }

})

export default Depense